import React, { Component } from 'react';
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import './App.css';
import TaskList from './todo/TaskList'
import InputTask from "./todo/InputTask";
import { Provider } from 'react-redux'
import {createStore, combineReducers, applyMiddleware } from 'redux'

export const put = (x, y) => ({ type: 'PUT', payload: x, payload2: y })

export const numberReducer = (state = 0, action) => {
  switch (action.type) {
    case 'ADD':
      return state + 1
    case 'MINUS':
      return state - 1
    default:
      return state
  }
}
const initState = {
  tasks: [{ id: 1, task: 'Programming', task2: 'GT Studio co.,ltd.'  }, { id: 2, task: 'Maintenance', task2: 'GT Studio co.,ltd.'  }],
}

export const taskReducer = (state = initState, action) => {
  switch (action.type) {
    case 'PUT':
      return state = {
        ...state,
        tasks: [...state.tasks, { id: state.tasks.id += 1, task: action.payload, task2: action.payload2 }]
      }
    default:
      return state
  }


}

export const rootReducer = combineReducers({tasks: taskReducer})
export const store = createStore(rootReducer, applyMiddleware(logger,thunk))

export default class App extends Component {
  state = {
    tasks: [
      { id: 1, task: 'Do homework', task2: 'Do homework' },
      { id: 2, task: 'Read book' , task2: 'Do homework'}],
    id: 3
  }
  render() {
    return (
      <div className="App">
      <header className="App-header">
      <br/>
         <h1>Todo App (Redux)</h1>
        <br/> </header>
        <br/>
        <h2>TODO List</h2> <br/> 
        <Provider store={store}>
         
          <TaskList tasks={this.state.tasks} />
          <br/>  <h2>ADD TODO </h2> <br/> 
          <InputTask addTask={this.addTask} id={this.state.id} />
          <br /></Provider>


      </div>
    );
  }
}

